part of 'chart_candle_layer.dart';

class ChartCandleSettings {
  final double radius;
  final double thickness;

  const ChartCandleSettings({
    this.radius = 4.0,
    this.thickness = 8.0,
  });
}
