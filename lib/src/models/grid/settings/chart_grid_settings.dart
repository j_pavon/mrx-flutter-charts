part of '../chart_grid_layer.dart';

class ChartGridSettings {
  final ChartGridSettingsAxis x;
  final ChartGridSettingsAxis y;

  const ChartGridSettings({
    required this.x,
    required this.y,
  });
}
