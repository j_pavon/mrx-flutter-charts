part of '../chart_axis_layer.dart';

class ChartAxisSettings {
  final ChartAxisSettingsAxis x;
  final ChartAxisSettingsAxis y;

  const ChartAxisSettings({
    required this.x,
    required this.y,
  });
}
