part of 'chart_bar_layer.dart';

class ChartBarSettings {
  final BorderRadius radius;
  final double thickness;

  const ChartBarSettings({
    this.thickness = 4.0,
    this.radius = BorderRadius.zero,
  });
}
