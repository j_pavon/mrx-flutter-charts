part of 'chart_line_layer.dart';

class ChartLineSettings {
  final Color color;
  final double thickness;

  const ChartLineSettings({
    required this.color,
    required this.thickness,
  });
}
