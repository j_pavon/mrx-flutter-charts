part of 'chart_group_bar_layer.dart';

class ChartGroupBarSettings {
  final double paddingBetweenItems;
  final BorderRadius radius;
  final double thickness;

  const ChartGroupBarSettings({
    this.paddingBetweenItems = 2.0,
    this.radius = BorderRadius.zero,
    this.thickness = 4.0,
  });
}
