part of 'chart_group_pie_layer.dart';

class ChartGroupPieSettings {
  final double angleOffset;
  final double gapBetweenChartCircles;
  final double gapSweepAngle;
  final double radius;
  final double thickness;

  const ChartGroupPieSettings({
    this.angleOffset = 30.0,
    this.gapBetweenChartCircles = 20.0,
    this.gapSweepAngle = 10.0,
    this.radius = 4.0,
    this.thickness = 8.0,
  });
}
